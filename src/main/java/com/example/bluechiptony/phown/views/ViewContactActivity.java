package com.example.bluechiptony.phown.views;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.bluechiptony.phown.R;
import com.example.bluechiptony.phown.model.Contact;

public class ViewContactActivity extends ActionBarActivity {

    public Contact reCon;


    public ViewContactActivity(Contact reCon) {
        this.reCon = reCon;
    }

    public ViewContactActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact);
        loadContactDetails();
    }


    public void loadContactDetails(){

        Intent intent = getIntent();
        String theName = intent.getStringExtra("GName");
        Contact gCon = (Contact)intent.getSerializableExtra("Conta");
        EditText fnam = (EditText)findViewById(R.id.firstNameText);
        EditText lname, phone1, phone2, email, address, pcode;

        lname = (EditText)findViewById(R.id.lastNameText);
        phone1 = (EditText)findViewById(R.id.phoneNumberText);
        email = (EditText)findViewById(R.id.email_addy);
        //email = (EditText)findViewById(R.id.em);
        address = (EditText)findViewById(R.id.addressText);



        fnam.setEnabled(false);
        lname.setEnabled(false);

        fnam.setText(gCon.getFirst_name());
        lname.setText(gCon.getLast_name());
        phone1.setText(gCon.getPhone_numbers());
        email.setText(gCon.getEmail_address());
        address.setText(gCon.getAddress());


        ImageView conImage = (ImageView) findViewById(R.id.imageView);


        conImage.setImageResource(R.drawable.funksmile);

        setTitle(gCon.getFirst_name()+ " "+ gCon.getLast_name());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
