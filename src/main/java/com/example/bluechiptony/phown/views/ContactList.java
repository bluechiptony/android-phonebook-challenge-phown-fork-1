package com.example.bluechiptony.phown.views;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.view.MenuInflater;
import com.example.bluechiptony.phown.R;
import com.example.bluechiptony.phown.helpers.ContactListAdapter;
import com.example.bluechiptony.phown.model.Contact;


public class ContactList extends ActionBarActivity {





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);


        Contact [] contaks = new Contact[10];

        //statc initialize elements

        contaks[0] = new Contact("James", "Dean", "09876778654", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[1] = new Contact("Rico", "Suave", "078456772634", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[2] = new Contact("Jack", "Alexis", "09848903909", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[3] = new Contact("Juan", "Marquez", "09873398654", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[4] = new Contact("Mellisa", "Milano", "08834678652", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[5] = new Contact("James", "Mollan", "098767328897",  "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[6] = new Contact("Alexander", "Estebaros", "0387637655",  "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[7] = new Contact("Rachael", "Malone", "09876778654", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[8] = new Contact("Cameron", "Dean", "09876778654",  "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[9] = new Contact("Jamal", "Henderson", "09876778654", "jadear@hoopla.com", "44 holloway drive, ellington");



        //configure list
        ListAdapter ladt = new ContactListAdapter(this, contaks);
        ListView contlist = (ListView) findViewById(R.id.ConList);
        contlist.setAdapter(ladt);

        registerForContextMenu(contlist);

        /*
        addBut.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent addIntent = new Intent(ContactList.this, AddContact.class);

                        addIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(addIntent);
                    }
                }
        );*/



        //configure on item click for list view
        contlist.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String clname = String.valueOf(parent.getItemAtPosition(position));

                        Contact pCon = (Contact)parent.getItemAtPosition(position);
                        //Toast.makeText(MainScreen.this, "Wahgwan "+clname, Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(ContactList.this, ViewContactActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("GName", clname);
                        intent.putExtra("Conta", (java.io.Serializable) pCon);
                        startActivity(intent);
                    }
                }
        );


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_list, menu);
        return true;
    }


    //added to create menu for long press

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Select Your Action");
        menu.add(0, v.getId(), 0, "Call");
        menu.add(0, v.getId(), 0, "Make Appointment");
        menu.add(0, v.getId(), 0, "Edit");
        menu.add(0, v.getId(), 0, "Delete");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        String toastMsg;




        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id == R.id.add_contact_button){
            openAddActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contact_list, menu);

        super.onOptionsMenuClosed(menu);
    }



    public void openAddActivity(){
        Intent addIntent = new Intent(ContactList.this, AddContact.class);

        addIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(addIntent);
    }
}
