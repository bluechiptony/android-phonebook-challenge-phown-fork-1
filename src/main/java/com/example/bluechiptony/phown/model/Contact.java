package com.example.bluechiptony.phown.model;

import java.io.Serializable;

/**
 * Created by BluechipTony on 07/03/2015.
 */
public class Contact implements Serializable {
    private String first_name;
    private String last_name;
    private String phone_numbers;
    private String email_address;
    private String address;

    public Contact(String first_name, String last_name, String phone_numbers, String email_address, String address) {
        this.email_address = email_address;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone_numbers = phone_numbers;
        this.address = address;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone_numbers() {
        return phone_numbers;
    }

    public void setPhone_numbers(String phone_numbers) {
        this.phone_numbers = phone_numbers;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
