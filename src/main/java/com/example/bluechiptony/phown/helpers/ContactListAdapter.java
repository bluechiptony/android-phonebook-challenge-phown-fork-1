package com.example.bluechiptony.phown.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bluechiptony.phown.R;
import com.example.bluechiptony.phown.model.Contact;

/**
 * Created by BluechipTony on 07/03/2015.
 */
public class ContactListAdapter extends ArrayAdapter<Contact> {
    public ContactListAdapter(Context context, Contact[] contaks) {
        super(context, R.layout.contact_list_row, contaks);


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater customInflator = LayoutInflater.from(getContext());
        View customView = customInflator.inflate(R.layout.contact_list_row, parent, false);

        Contact con = getItem(position);

        //TextView nameText = (TextView) customView.findViewById(R.id.);
        TextView nameText = (TextView) customView.findViewById(R.id.contakName);
        TextView phoneText = (TextView) customView.findViewById(R.id.phoneDispText);
        ImageView conImage = (ImageView) customView.findViewById(R.id.contImage);




        nameText.setText(con.getFirst_name()+ " "+con.getLast_name());
        phoneText.setText(con.getPhone_numbers());
        conImage.setImageResource(R.drawable.funksmile);


        return customView;


    }
}
