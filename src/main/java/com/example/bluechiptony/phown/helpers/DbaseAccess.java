package com.example.bluechiptony.phown.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BluechipTony on 07/03/2015.
 */
public class DbaseAccess extends SQLiteOpenHelper {

    private static final int  database_version = 1;

    private static final String database_name = "PhownbookContacts";

    private static final String contacts_table = "ContactsTable";

    private static final String KEY_ID = "id";

    private static final String KEY_FIRST_NAME = "first_name";
    private static final String KEY_LAST_NAME = "last_name";
    private static final String KEY_PHONE_NUMBER = "phone_number";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_ADDRESS = "address";





    public DbaseAccess(Context context){
        super(context, database_name, null, database_version);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String create_table = "CREATE TABLE "+ contacts_table + "(" + KEY_ID +" INTEGER PRIMARY KEY, "
                + KEY_FIRST_NAME +" TEXT,"
                + KEY_LAST_NAME + " TEXT,"
                + KEY_PHONE_NUMBER + " TEXT,"
                + KEY_EMAIL + " TEXT,"
                + KEY_ADDRESS + " TEXT,"+")";


        db.execSQL(create_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String drop_table = "DROP TABLE IF EXISTS "+database_name;
        try {
            db.execSQL(drop_table);


        }catch (Exception e){

        }

        onCreate(db);

    }


}
